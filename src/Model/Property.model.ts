export class Property {
  public Id: number;
  public Name: string;
  public TotalRentalArea: number;
}

export class Tenant {
  public Id: number;
  public Name: string;
  public Area: number;
}
