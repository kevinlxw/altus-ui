import { Component, OnInit, OnChanges, SimpleChange } from "@angular/core";
import { AltusHttpService } from "src/service/http.service";
import { Property, Tenant } from "src/Model/Property.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  public property: Property;
  public tenant: Tenant;
  public occupancy: number;
  constructor(private webservice: AltusHttpService) {
    this.property = { Id: 0, Name: "", TotalRentalArea: 0 };
    this.tenant = { Id: 1, Name: "", Area: 0 };
  }

  ngOnInit() {
    this.webservice.get<Property>("1").subscribe(res => {
      this.property = res;
    });

    this.webservice.get<Tenant>("tenant/111").subscribe(res => {
      this.tenant = res;
    });
  }
}
