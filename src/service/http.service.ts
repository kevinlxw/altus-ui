import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Property } from "src/Model/Property.model";

@Injectable({
  providedIn: "root"
})
export class AltusHttpService {
  private url: string = "http://localhost:8080/api/property/";
  private header: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*"
  });

  constructor(private http: HttpClient) {}

  private async GetData() {
    var dd = await this.http.get<Property>(this.url);
  }
  public get<T>(query: string) {
    return this.http.get<T>(this.url + query, { headers: this.header });
  }
}
